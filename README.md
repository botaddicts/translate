# 🗣 translate
![screenshot](Put a screenshot here)

Translating stuff directly from discord

## Requirements
* Python3
* Linux system with systemd
* Discord bot (with intents for messages enabled)

## Installation

Clone the repository, go in it, install nextcord, copy the template config file, edit it and run the script.

```bash
git clone https://codeberg.org/botaddicts/translate
cd translate/
pip install -r requirements.txt
cp config.template.py config.py
nano config.py
python3 main.py
```

And for systemd: (running in the background):

```bash
sed -i "s|ABSOLUTE_PATH|$(pwd)|g" translate.service
sed -i "s|USER|$USER|g" translate.service
sudo cp translate.service /etc/systemd/system/
sudo systemctl start translate
```

## Usage
Just run `/trans <a string to translate into english here>` and that's it!
