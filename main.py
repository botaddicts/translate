import nextcord, requests
from config import token, ids, url

import logging
logging.basicConfig(level=logging.INFO)

bot = nextcord.Client()

# Stuff for translation
data = {'q': 'Coucou, ça va ?', 'source': 'auto', 'target': 'en', 'format': 'text'}

@bot.slash_command(description="Translate something", guild_ids=ids)
async def trans(interaction, message:str = nextcord.SlashOption(name="text", description="Enter text to translate.", required=True)):
    data['q'] = message
    x = requests.post(url, data=data)
    translated = x.json()['translatedText']
    await interaction.response.send_message(translated, ephemeral=True)
    data['q'] = "N/A"

bot.run(token)
